public class ComplexTest {
	public static void main(String[] args) {
		// set/get, constructor
		Complex c1 = new Complex();
		c1.setReal(1.0);
		c1.setImag(2.0);
		Complex c2 = new Complex(10.4, 20.2);

		System.out.printf("c1: real: %.2f, imag: %.2f%n", c1.getReal(), c1.getImag());

		// to String
		System.out.println(c2);

		// equals
		System.out.printf("should be false: %b%n", c1.equals(c2));
		c1.setReal(c2.getReal());
		c1.setImag(c2.getImag());
		System.out.printf("should be true: %b%n", c1.equals(c2));

		String str = "";
		System.out.printf("should be false: %b%n", c1.equals(str));

		// multply inherent test of object method
		Complex c3 = new Complex(2.1, 4.7);
		System.out.printf("%s * %s = %s%n", c1, c3, Complex.multiply(c1, c3).toString());
	}

}
