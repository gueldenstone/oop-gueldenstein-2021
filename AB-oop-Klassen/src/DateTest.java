public class DateTest {
	public static void main(String[] args) {
		// test std constructor
		Date d1 = new Date();

		// test parameterized constructor
		Date d2 = new Date(3, 5, 2021);

		// test toString()
		System.out.println(d1);
		System.out.println(d2);

		// setters
		d1.setDay(2);
		d1.setMonth(2);
		d1.setYear(1971);
		System.out.println(d1);

		// getters
		System.out.printf("day: %d, month %d, year %d\n", d2.getDay(), d2.getMonth(), d2.getYear());

		// equals
		System.out.printf("should be false: %b\n", d1.equals(d2));

		d1.setDay(3);
		d1.setMonth(5);
		d1.setYear(2021);
		System.out.printf("should be true: %b\n", d1.equals(d2));

		String str = "Hello";
		System.out.printf("should be false: %b\n", d1.equals(str));

		// test quarter
		System.out.printf("Quarter of %s is %d%n", d1.toString(), Date.quarter(d1));

	}
}
