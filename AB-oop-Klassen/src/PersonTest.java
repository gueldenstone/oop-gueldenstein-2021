
public class PersonTest {

	public static void main(String[] args) {

		Person p1 = new Person("Lukas", new Date(5, 12, 1994));
		Person p2 = new Person("Riv", new Date(29, 11, 1995));
		Person p3 = new Person("Paul", new Date(2, 1, 2000));

		System.out.printf("Hi, my name is %s, I was born on %s%n", p1.getName(), p1.getBirthday());
		System.out.printf("instance called '%s' has ID %d%n", p1.getName(), p1.getUid());
		System.out.printf("Hi, my name is %s, I was born on %s%n", p2.getName(), p2.getBirthday());
		System.out.printf("instance called '%s' has ID %d%n", p2.getName(), p2.getUid());
		p2.setName("Becky");
		System.out.printf("Hi, my name is %s, I was born on %s%n", p2.getName(), p2.getBirthday());
		System.out.printf("instance called '%s' has ID %d%n", p2.getName(), p2.getUid());

		System.out.printf("there have been %d instantiation of class 'Person'%n", Person.getNumInstances());

	}
}
