public class Person {

	private String name;
	private Date birthday;
	private int uid;
	private static int numInstances = 0;

	public Person(String name, Date d) {
		setName(name);
		this.birthday = d;
		numInstances++;
		this.uid = numInstances;
	}

	public String getName() {
		return this.name;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static int getNumInstances() {
		return numInstances;
	}

	public int getUid() {
		return this.uid;
	}
}
