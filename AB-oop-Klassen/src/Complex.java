public class Complex {
	private double real;
	private double imag;

	public Complex() {
		setReal(0.0);
		setImag(0.0);
	}

	public Complex(double r, double i) {
		setReal(r);
		setImag(i);
	}

	public double getReal() {
		return this.real;
	}

	public double getImag() {
		return this.imag;
	}

	public void setReal(double r) {
		this.real = r;
	}

	public void setImag(double i) {
		this.imag = i;
	}

	@Override
	public String toString() {
		return String.format("(%.2f) + j(%.2f)", this.getReal(), this.getImag());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Complex) {
			Complex c = (Complex) obj;
			return c.getReal() == this.real && c.getImag() == this.imag;
		}
		return false;
	}

	public Complex multiplyWith(Complex c) {
		if (c == null)
			return new Complex();
		return new Complex(this.real * c.getReal() - this.imag * c.getImag(),
				this.real * c.getImag() + this.imag * c.getReal());
	}

	public static Complex multiply(Complex c1, Complex c2) {
		if (c1 == null && c2 == null)
			return new Complex();
		return c1.multiplyWith(c2);

	}
}