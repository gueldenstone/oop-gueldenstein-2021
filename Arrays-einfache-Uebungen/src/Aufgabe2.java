public class Aufgabe2 {
	public static void main(String[] args) {
		int[] liste = new int[10];
		for (int i = 0; i < liste.length; ++i) {
			liste[i] = 2 * i + 1;
		}
		for (int val : liste) {
			System.out.println(val);
		}

	}
}
