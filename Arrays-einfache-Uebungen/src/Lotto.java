public class Lotto {

	public static void main(String[] args) {
		int[] lottoZahlen = { 3, 7, 12, 18, 37, 42 };
		int[] pruefen = { 12, 13 };

		// Ausgabe\
		System.out.print("[ ");
		for (int val : lottoZahlen) {
			System.out.print(val + " ");
		}
		System.out.print("]\n");

		// Zahlen pruefen
		for (int p : pruefen) {

			if (contains(lottoZahlen, p)) {
				System.out.printf("Die Zahl %d ist in der Ziehung enthalten.\n", p);
			} else {
				System.out.printf("Die Zahl %d ist in der Ziehung nicht enthalten.\n", p);
			}
		}
	}

	// returns true if found
	public static boolean contains(int[] array, int key) {
		for (int val : array) {
			if (val == key) {
				return true;
			}
		}
		return false;
	}
}
