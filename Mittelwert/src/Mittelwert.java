import java.util.Scanner;

public class Mittelwert {

	public static void main(String[] args) {

		// (E) "Eingabe"
		// Werte für x und y festlegen:
		// ===========================
		double x = doubleEinlesen("Geben Sie eine Zahl ein");
		double y = doubleEinlesen("Geben Sie eine Zahl ein");
		double m;

		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		// m = (x + y) / 2.0;
		m = mittelwertBerechnen(x, y);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		// System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
		printResult(x, y, m);
	}

	public static double mittelwertBerechnen(double z1, double z2) {
		double ergebnis;
		ergebnis = (z1 + z2) / 2;
		return ergebnis;
	}

	public static double doubleEinlesen(String msg) {
		Scanner input = new Scanner(System.in);
		System.out.println(msg);
		double ergebnis = input.nextDouble();
		input.close();
		return ergebnis;
	}

	public static void printResult(double x, double y, double z) {
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, z);
	}
}
