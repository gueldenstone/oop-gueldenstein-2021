import java.util.Scanner;

public class Quersumme {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		System.out.print("enter a number: ");
		int num = input.nextInt();
		System.out.println("\n-------------");

		int sum = 0;
		while (num != 0) {
			sum += num % 10;
			num = num / 10;
		}

		System.out.printf("the checksum is %d\n", sum);
		input.close();
	}
}
