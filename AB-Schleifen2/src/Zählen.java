import java.util.Scanner;

public class Zählen {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		System.out.println("please enter the number to which i will count");
		int n = input.nextInt();

		int i = 1;
		while (i <= n) {
			System.out.println(i);
			i++;
		}

		input.close();

	}
}
