import java.util.*;

public class CollectionTest {

	public static Book findBook(List<Book> bookCollection, String isbn) {
		for (Book b : bookCollection) {
			if (b.getISBN().equals(isbn)) {
				return b;
			}
		}
		return null;
	}

	public static boolean removeBook(List<Book> bookCollection, Book book) {
		return bookCollection.remove(book);
	}

	public static String findGreatestISBN(List<Book> bookCollection) {
		return Collections.max(bookCollection).getISBN();
	}

	public static void main(String[] args) {
		Book b1 = new Book("Max Czollek", "Desintegriert euch!", "1742");
		Book b2 = new Book("Lukas Güldenstein", "Ein Buch", "7326982");
		Book b3 = new Book("Riv", "Verschwörungsmythen", "1234");

		// test with arrayList
		ArrayList<Book> bookCollectionArray = new ArrayList<Book>();
		bookCollectionArray.add(b1);
		bookCollectionArray.add(b2);
		bookCollectionArray.add(b3);
		System.out.println(bookCollectionArray);

		System.out.println("found: " + findBook(bookCollectionArray, "1234"));

		if (removeBook(bookCollectionArray, b1)) {
			System.out.println("book removed!");
		}
		System.out.println(bookCollectionArray);

		System.out.println("greatest ISBN in collection: " + findGreatestISBN(bookCollectionArray));

		System.out.println(bookCollectionArray);

		System.out.println("-------------------------------------------------");
		// test with linked list
		LinkedList<Book> bookCollectionLinked = new LinkedList<Book>();
		bookCollectionLinked.add(b1);
		bookCollectionLinked.add(b2);
		bookCollectionLinked.add(b3);
		System.out.println(bookCollectionLinked);

		System.out.println("found: " + findBook(bookCollectionLinked, "1234"));

		if (removeBook(bookCollectionLinked, b1)) {
			System.out.println("book removed!");
		}
		System.out.println(bookCollectionLinked);

		System.out.println("greatest ISBN in collection: " + findGreatestISBN(bookCollectionLinked));

		System.out.println(bookCollectionLinked);
	}
}
