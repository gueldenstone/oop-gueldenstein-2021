public class Book implements Comparable<Book> {
	private String author;
	private String title;
	private String isbn;

	public Book(String author, String title, String isbn) {
		setAuthor(author);
		setTitle(title);
		setISBN(isbn);
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getISBN() {
		return this.isbn;
	}

	public void setISBN(String isbn) {
		this.isbn = isbn;
	}

	@Override
	public int compareTo(Book book) {
		return isbn.compareTo(book.getISBN());
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Book) {
			Book b = (Book) o;
			return isbn.equals(b.getISBN());
		}
		return false;
	}

	@Override
	public String toString() {
		return "{" + " author='" + getAuthor() + "'" + ", title='" + getTitle() + "'" + ", isbn='" + getISBN() + "'" + "}";
	}

	public static void main(String[] args) {
		Book b1 = new Book("Max Czollek", "Desintegriert euch", "3446260277");
		Book b2 = new Book("Max Czollek", "Desintegriert euch", "3446260279");
		// Book b1 = new Book();
		System.out.println(b1.equals(b2));
	}

}
