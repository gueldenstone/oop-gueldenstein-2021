import java.util.*;

public class TreeSetTest {
	public static void main(String[] args) {
		TreeSet<Book> ts = new TreeSet<Book>();
		Book b1 = new Book("Max Czollek", "Desintegriert euch!", "1742");
		Book b2 = new Book("Lukas Güldenstein", "Ein Buch", "7326982");
		Book b3 = new Book("Riv", "Verschwörungsmythen", "1234");

		System.out.printf("should be true: %b%n", ts.add(b1));
		System.out.printf("should be true: %b%n", ts.add(b2));
		System.out.printf("should be true: %b%n", ts.add(b3));
		System.out.printf("should be false: %b%n", ts.add(b3));

	}
}
