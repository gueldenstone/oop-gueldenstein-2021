public class Ticket {
	private String bezeichner;
	private double preis;
	private int anzahl;

	public Ticket() {
		this.bezeichner = "ticket";
		this.preis = 0.0;
		this.anzahl = 1;
	}

	public Ticket(String bezeichner, double preis, int anzahl) {
		this.bezeichner = bezeichner;
		this.preis = preis;
		this.anzahl = anzahl;
	}

	public String getBezeichner() {
		return this.bezeichner;
	}

	public void setBezeichner(String bezeichner) {
		this.bezeichner = bezeichner;
	}

	public double getPreis() {
		return this.preis;
	}

	public void setPreis(double preis) {
		if (preis < 0)
			this.preis = 0;
		else
			this.preis = preis;
	}

	public int getAnzahl() {
		return this.anzahl;
	}

	public void setAnzahl(int anzahl) {
		if (anzahl < 0)
			this.anzahl = 0;
		else
			this.anzahl = anzahl;
	}

	@Override
	public String toString() {
		return "{" + " bezeichner='" + getBezeichner() + "'" + ", preis='" + getPreis() + "'" + ", anzahl='" + getAnzahl()
				+ "'" + "}";
	}

}