public class TestTicket {
	public static void main(String[] args) {
		// test constructors
		Ticket t1 = new Ticket();
		Ticket t2 = new Ticket("Fahrkarte 2", 1.0, 2);

		System.out.println(t1);
		System.out.println(t2);

		// setters
		t1.setBezeichner("Fahrkarte 1");
		t1.setPreis(-100.0);
		t1.setAnzahl(-1);
		System.out.println(t1);
		t1.setPreis(100.0);
		t1.setAnzahl(1);
		System.out.println(t1);

		// getters
		System.out.printf("Bezeichner: %s, Preis %.2f, Anzahl: %d%n", t2.getBezeichner(), t2.getPreis(), t2.getAnzahl());

		// to String tested inherently
	}
}
