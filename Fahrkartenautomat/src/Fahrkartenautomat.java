﻿import java.util.Scanner;
import java.lang.Math;

class Fahrkartenautomat {

	private static Ticket[] ticketarten = { new Ticket("Einzelfahrschein Berlin AB", 2.9, 0),
			new Ticket("Einzelfahrschein Berlin BC", 3.3, 0), new Ticket("Einzelfahrschein Berlin ABC", 3.6, 0),
			new Ticket("Kurzstrecke", 1.9, 0), new Ticket("Tageskarte Berlin AB", 8.6, 0),
			new Ticket("Tageskarte Berlin BC", 9.0, 0), new Ticket("Tageskarte Berlin ABC", 9.6, 0),
			new Ticket("Kleingruppen-Tageskarte Berlin AB", 23.5, 0),
			new Ticket("Kleingruppen-Tageskarte Berlin BC", 24.3, 0),
			new Ticket("Kleingruppen-Tageskarte Berlin ABC", 24.9, 0), };

	public Fahrkartenautomat() {
	}

	public static double fahrkartenbestellungErfassen() {
		// reset anzahl
		for (int i = 0; i < (ticketarten.length); ++i) {
			ticketarten[i].setAnzahl(0);
		}
		double gesamtPreis = 0.0;
		int auswahl;
		System.out.println(" Fahrkartenbestellvorgang:");
		System.out.println(" =========================");
		do {
			auswahl = fahrkartenMenue();

			if (auswahl == 0)
				for (int i = 0; i < ticketarten.length; i++)
					gesamtPreis += ticketarten[i].getPreis() * ticketarten[i].getAnzahl();
			else
				erfasseTicketAnzahl(auswahl);

		} while (auswahl != 0);

		return gesamtPreis;

	}

	public static void erfasseTicketAnzahl(int auswahl) {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("\n  Anzahl der Tickets: ");
		ticketarten[auswahl - 1].setAnzahl(tastatur.nextInt());
	}

	public static int fahrkartenMenue() {
		int auswahl;
		boolean istEingabeKorrekt = false;
		Scanner tastatur = new Scanner(System.in);

		do {
			System.out.println("\n  Wählen Sie :");
			for (int i = 0; i < ticketarten.length; i++)
				System.out.printf("\t(%d)\t%s, %.2f%n", (i + 1), ticketarten[i].getBezeichner(), ticketarten[i].getPreis());
			System.out.println("\t(0)\tBezahlen\n");
			System.out.print("  Ihre Wahl: ");
			auswahl = tastatur.nextInt();
			if (auswahl < ticketarten.length + 1 && auswahl >= 0)
				istEingabeKorrekt = true;
			else
				System.out.println("  >>falsche Eingabe<<");

		} while (!istEingabeKorrekt);
		return auswahl;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfeneMünze;
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.format("  Noch zu zahlen: %4.2f € %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("  Eingabe (mind. 5Ct, höchstens 10 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\n  Fahrschein wird ausgegeben");
		System.out.print("  ");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(255);
		}
		System.out.println("\n");
	}

	public static void warte(int milisekunde) {
		try {
			Thread.sleep(milisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void rückgeldAusgeben(double rückgabebetrag) {

		if (rückgabebetrag > 0.0) {
			System.out.format("  Der Rückgabebetrag in Höhe von %4.2f € %n", rückgabebetrag);
			System.out.println("  wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) {// 2 EURO-Münzen
				münzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
				rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
			}
			while (rückgabebetrag >= 1.0) {// 1 EURO-Münzen
				münzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
				rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				münzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
				rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				münzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
				rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Müzen
			{
				münzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
				rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				münzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
				rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
			}
		}
	}

	public static void münzeAusgeben(int betrag, String einheit) {

		System.out.println("                 * * *        ");
		System.out.println("               *       *      ");
		System.out.format("              *    %-2s   *     %n", betrag);
		System.out.format("              *   %4s  *     %n", einheit);
		System.out.println("               *       *      ");
		System.out.println("                 * * *        ");

	}

	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double rückgabebetrag;
		do {

			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rückgeldAusgeben(rückgabebetrag);

			System.out.println("\n  Vergessen Sie nicht, den Fahrschein\n" + "  vor Fahrtantritt entwerten zu lassen!\n"
					+ "  Wir wünschen Ihnen eine gute Fahrt.\n\n");
		} while (true);

	}
}