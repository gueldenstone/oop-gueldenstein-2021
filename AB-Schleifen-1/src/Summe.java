import java.util.Scanner;

public class Summe {

	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		System.out.print("Geben Sie bitte einen begrenzenden Wert ein: ");
		int n = input.nextInt();
		System.out.println("\n------------");

		// c
		int sum = 0;
		for (int i = 1; i <= n; i += 2) {
			sum += i;
		}
		System.out.printf("Die Summe für C beträgt: %d", sum);
	}

}
