public class Date {
	// attributes
	private int day;
	private int month;
	private int year;

	// standar constructor
	public Date() {
		setDay(1);
		setMonth(1);
		setYear(1970);
	}

	// init values with constructor
	public Date(int day, int month, int year) {
		setDay(day);
		setMonth(month);
		setYear(year);
	}

	// getters
	public int getDay() {
		return this.day;
	}

	public int getMonth() {
		return this.month;
	}

	public int getYear() {
		return this.year;
	}

	// setters
	public void setDay(int day) {
		if (day < 1 || day > 31)
			this.day = 1;
		else
			this.day = day;
	}

	public void setMonth(int month) {
		if (month < 1 || month > 12)
			this.month = 1;
		else
			this.month = month;
	}

	public void setYear(int year) {
		if (year < 0)
			this.year = 0;
		this.year = year;
	}

	// overloads
	@Override
	public String toString() {
		return String.format("%02d.%02d.%04d", getDay(), getMonth(), getYear());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Date) {
			Date d = (Date) obj;
			return this.day == d.getDay() && this.month == d.getMonth() && this.year == d.getYear();
		}
		return false;
	}

	static int quarter(Date d) {
		// check null pointer
		if (d == null) {
			// ? throw here
			return 0;
		}
		// determine qurter of the year
		if (d.getMonth() <= 3) {
			return 1;
		} else if (d.getMonth() <= 6) {
			return 2;
		} else if (d.getMonth() <= 9) {
			return 3;
		} else {
			return 4;
		}
	}
}
