public class BankaccountTest {
	public static void main(String[] args) {
		Bankaccount acc1 = new Bankaccount();
		Bankaccount acc2 = new Bankaccount("Lukas", 100.0);
		Bankaccount acc3 = new Bankaccount("Max", 10.0);

		System.out.println(acc1);
		System.out.println(acc2);

		acc1.setOwner("Riv");
		acc1.deposit(50);
		System.out.println(acc1);
		System.out.printf("paying out %.2f%n", acc2.withdraw(5));
		System.out.println(acc2);

		System.out.println(acc3);
		System.out.printf("balance is: %.2f, paying out %.2f%n", acc3.getBalance(), acc3.withdraw(15));
	}
}
