public class Holiday extends Date {
	private String name;

	public Holiday(int day, int month, int year, String name) {
		super(day, month, year);
		setName(name);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("%s (%s)", super.toString(), this.getName());
	}

}
