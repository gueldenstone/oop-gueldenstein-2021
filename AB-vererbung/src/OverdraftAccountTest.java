public class OverdraftAccountTest {
	public static void main(String[] args) {
		OverdraftAccount acc1 = new OverdraftAccount("Lukas", 60.0, 100.0);
		OverdraftAccount acc2 = new OverdraftAccount("Riv", -40.0, 100.0);

		// check if withdrawing works
		System.out.printf("paying out %.2f, account is: %s%n", acc1.withdraw(170), acc1);
		System.out.printf("paying out %.2f, account is: %s%n", acc2.withdraw(50), acc2);

		Bankaccount acc3 = new Bankaccount("Hamid", 100.00);
		Bankaccount acc4 = new Bankaccount("Elena", 100.00);

		Bankaccount[] accounts = { acc1, acc2, acc3, acc4 };

		for (int i = 0; i < accounts.length; i++) {
			System.out.println(accounts[i]);
		}
	}
}
