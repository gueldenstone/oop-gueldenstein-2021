public class OverdraftAccount extends Bankaccount {
	private double overdraft;

	public OverdraftAccount() {
		super();
		setOverdraft(0);
	}

	public OverdraftAccount(String owner, double balance, double overdraft) {
		super(owner, balance);
		setOverdraft(overdraft);
	}

	public double getOverdraft() {
		return this.overdraft;
	}

	public void setOverdraft(double overdraft) {
		this.overdraft = overdraft;
	}

	@Override
	public double withdraw(double amount) {
		// withdraw to much
		double withdraw = 0.0;
		if (getBalance() + getOverdraft() - amount < 0) {
			withdraw = getBalance() + getOverdraft();
		} else {
			withdraw = amount;
		}
		setBalance(getBalance() - withdraw);
		return withdraw;

	}

	@Override
	public String toString() {
		return String.format("[ %s, overdraft: %.2f ]", super.toString(), getOverdraft());
	}

}
