public class Bankaccount {
	private String owner;
	private long accountID;
	private double balance;
	private static long accountIDCnt = 0;

	public Bankaccount() {
		setOwner("unknown");
		setBalance(0.0);
		accountID = accountIDCnt++;
	}

	public Bankaccount(String owner, double balance) {
		setOwner(owner);
		setBalance(balance);
		accountID = accountIDCnt++;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public long getAccountID() {
		return this.accountID;
	}

	public double getBalance() {
		return this.balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void deposit(double amount) {
		this.balance += amount;
	}

	public double withdraw(double amount) {
		this.balance -= amount;
		return amount;
	}

	@Override
	public String toString() {
		return String.format("[ owner: %s, accountID: %d, balance: %.2f ]", getOwner(), getAccountID(), getBalance());
	}

}