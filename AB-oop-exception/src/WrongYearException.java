public class WrongYearException extends WrongDateException {
	public WrongYearException(String msg, int wrongValue) {
		super(msg, wrongValue);
	}
}
