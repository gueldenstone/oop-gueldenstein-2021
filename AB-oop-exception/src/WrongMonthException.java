public class WrongMonthException extends WrongDateException {
	public WrongMonthException(String msg, int wrongValue) {
		super(msg, wrongValue);
	}
}
