
public class Datum {
	private int tag;
	private int monat;
	private int jahr;

	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}

	public Datum(int tag, int monat, int jahr) throws WrongDayException, WrongMonthException, WrongYearException {
		setTag(tag);
		setMonat(monat);
		setJahr(jahr);
	}

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) throws WrongDayException {
		if (tag < 1 || tag > 31) {
			throw new WrongDayException("falscher Tag", tag);
		}
		this.tag = tag;
	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) throws WrongMonthException {
		if (monat < 1 || monat > 12) {
			throw new WrongMonthException("falscher Monat", monat);
		}
		this.monat = monat;
	}

	public int getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) throws WrongYearException {
		if (jahr < 1900 || jahr > 2100) {
			throw new WrongYearException("falsches Jahr", jahr);
		}
		this.jahr = jahr;
	}

	public static int berechneQuartal(Datum d) {
		return d.monat / 3 + 1;
	}

	@Override
	public boolean equals(Object obj) {
		Datum d = (Datum) obj;
		if (this.tag == d.tag && this.monat == d.monat && this.jahr == d.jahr)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return this.tag + "." + this.monat + "." + this.jahr;
	}

	public static void main(String[] args) {
		try {
			Datum d1 = new Datum();
			Datum d2 = new Datum(-1, 100, 2500);
			d1.setJahr(1900);
			d1.setMonat(10);
			d1.setTag(1);

			System.out.println(d1);
			System.out.println(d2);

		} catch (WrongDayException ex) {
			System.out.println(ex);
		} catch (WrongMonthException ex) {
			System.out.println(ex);
		} catch (WrongYearException ex) {
			System.out.println(ex);
		}
	}
}
