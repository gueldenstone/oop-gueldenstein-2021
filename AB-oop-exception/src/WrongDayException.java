public class WrongDayException extends WrongDateException {
	public WrongDayException(String msg, int wrongValue) {
		super(msg, wrongValue);
	}
}
