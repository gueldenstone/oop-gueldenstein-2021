public class Division {
	public static void main(String[] args) {

		try {
			System.out.println(divide(10, 9));
			System.out.println(divide(10, 0));
		} catch (ArithmeticException ex) {
			System.out.println(ex);
		}
	}

	public static float divide(int dividend, int divisor) throws ArithmeticException {
		if (divisor == 0) {
			throw new ArithmeticException("Division durch Null");
		}
		return (float) dividend / (float) divisor;
	}
}
