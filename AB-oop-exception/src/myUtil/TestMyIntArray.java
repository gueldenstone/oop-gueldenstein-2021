package myUtil;

public class TestMyIntArray {
	public static void main(String[] args) {
		MyIntArray a = new MyIntArray();
		a.increase(10);

		try {
			for (int i = 0; i < 10; i++) {
				a.set(i, i + 1);
			}
		} catch (MyIndexException ex) {
			System.out.println(ex + "kann nicht setzen: index = " + ex.getWrongIndex());
		}

		System.out.println(a);

		try {
			for (int i = 10; i < 20; i++) {
				a.set(i, i + 1);
			}
		} catch (MyIndexException ex) {
			System.out.println(ex + "kann nicht setzen: index = " + ex.getWrongIndex());
		}

		try {
			System.out.println(a.get(20));
		} catch (MyIndexException ex) {
			System.out.println(ex + "kann nicht abrufen: index = " + ex.getWrongIndex());
		}
		a.increase(20);
		try {
			System.out.println(a.get(19));
		} catch (MyIndexException ex) {
			System.out.println(ex + "kann nicht abrufen: index = " + ex.getWrongIndex());
		}
	}
}
