package myUtil;

public class MyIntArray implements IIntArray {

	private int[] array;

	public MyIntArray() {
		array = new int[0];
	}

	/**
	 * Funktion liefert den Wert am angegebenen Index eines Arrays
	 *
	 * @param index der Index
	 * @return der Wert, der an index steht
	 * @throws MyIndexException, wenn index ungültig ist
	 */
	public int get(int index) throws MyIndexException {
		if (index < 0 || index > array.length - 1) {
			throw new MyIndexException(index);
		}
		return array[index];
	}

	/**
	 * Funktion setzt einen bestimmten Wert am angegebenen Index eines Arrays
	 *
	 * @param index der Index, dessen Wert gesetzt werden soll
	 * @param value der neue Wert
	 * @throws MyIndexException, wenn index ungültig ist
	 */
	public void set(int index, int value) throws MyIndexException {
		if (index < 0 || index > array.length - 1) {
			throw new MyIndexException(index);
		}
		array[index] = value;
	}

	/**
	 * Funktion zum Vergrößern eines Arrays
	 *
	 * @param n die Anzahl der Pl�tze, um die das Array vergrößert werden soll
	 */
	public void increase(int n) {
		// do not decrease array size
		if (n < 0) {
			n = 0;
		}
		int[] tmp = new int[this.array.length + n];
		for (int i = 0; i < array.length; i++) {
			tmp[i] = array[i];
		}
		array = tmp;
	}

	@Override
	public String toString() {
		String str = "[ ";
		for (int v : this.array) {
			str += v + ", ";
		}
		str += "]";
		return str;
	}
}