public abstract class WrongDateException extends Exception {
	private int wrongValue;

	protected WrongDateException(String msg, int wrongValue) {
		super(msg);
		this.wrongValue = wrongValue;
	}

	public int getWrongValue() {
		return this.wrongValue;
	}

	@Override
	public String getMessage() {
		return String.format("%s value: %d", super.getMessage(), getWrongValue());
	}
}
