public class TestStack {
	public static void main(String[] args) {
		Stack s = new Stack();

		// push 3 numbers
		s.push(2);
		s.push(3);
		s.push(4);

		System.out.println(s.pop());
		System.out.println(s.pop());
		System.out.println(s.pop());
		// pop one more
		System.out.println("should be error: " + s.pop());
		System.out.println("should be error: " + s.pop());
		System.out.println("should be error: " + s.pop());
		System.out.println("should be error: " + s.pop());

		int many = 299;
		// push too many
		for (int i = 1; i < many + 1; i++) {
			s.push(i);
		}

		// pop to print
		for (int i = 0; i < many; i++) {
			System.out.print(s.pop() + ", ");
		}

		// pop to print
		System.out.println("Länge: " + s.getLength());
	}
}
