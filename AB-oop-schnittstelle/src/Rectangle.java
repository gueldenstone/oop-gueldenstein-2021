public class Rectangle extends GeoObject {
	private double height;
	private double width;

	public Rectangle() {
		super(0.0, 0.0);
		this.height = 0.0;
		this.width = 0.0;
	}

	public Rectangle(double x, double y, double h, double w) {
		super(x, y);
		this.height = h;
		this.width = w;
	}

	public double getHeight() {
		return this.height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWidth() {
		return this.width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public

	/**
	 * Methode bestimmt die Flaeche des geometrischen Objekts
	 * 
	 * @return die Flaeche
	 */
	@Override double determineArea() {
		return this.height * this.width;
	}

	@Override
	public String toString() {
		return String.format("[ position: [ x=%.3f, y=%.3f ], height: %.3f, width: %.3f ]", this.getX(), this.getY(),
				this.getHeight(), getWidth());
	}
}
