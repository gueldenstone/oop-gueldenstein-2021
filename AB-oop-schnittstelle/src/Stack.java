public class Stack implements IntStack {
	public static int initLen = 100;
	private int[] memory;
	private int index;

	public Stack() {
		this.memory = new int[initLen];
		this.index = -1;
	}

	@Override
	public void push(int data) {
		// check if initLen has been reached
		if (this.index >= memory.length - 1) {
			// store stack
			int origLength = this.memory.length;
			int[] copy = new int[this.memory.length + initLen];
			for (int i = 0; i < origLength; i++) {
				copy[i] = this.memory[i];
			}
			// restore stack
			this.memory = copy;
		}
		// store in array
		this.memory[++this.index] = data;
	}

	@Override
	public int pop() {
		// check if last element
		if (this.index < 0) {
			// ! log error here
			return -1;
		}
		return this.memory[this.index--];
	}

	public int getLength() {
		return this.memory.length;
	}
}
