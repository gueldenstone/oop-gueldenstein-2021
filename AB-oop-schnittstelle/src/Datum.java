public class Datum implements Comparable<Datum> {
	private int tag;
	private int monat;
	private int jahr;

	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}

	public Datum(int tag, int monat, int jahr) {
		this.tag = tag;
		this.monat = monat;
		this.jahr = jahr;
	}

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) {
		this.monat = monat;
	}

	public int getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) {
		this.jahr = jahr;
	}

	public static int berechneQuartal(Datum d) {
		return d.monat / 3 + 1;
	}

	@Override
	public boolean equals(Object obj) {
		Datum d = (Datum) obj;
		if (this.tag == d.tag && this.monat == d.monat && this.jahr == d.jahr)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return this.tag + "." + this.monat + "." + this.jahr;
	}

	@Override
	public int compareTo(Datum o) {
		int d1 = this.getJahr() + this.getMonat() + this.getTag();
		int d2 = o.getJahr() + o.getMonat() + o.getTag();
		return d1 - d2;

		// // check if equal
		// if (this.equals(o)) {
		// return 0;
		// }
		// // check consecutively if any attribute of this object is larger
		// // meaning later in time, so return a positive value. otherwise return
		// negative
		// if (this.jahr > o.jahr) {
		// return 1;
		// } else if (this.jahr == o.jahr && this.monat > o.monat) {
		// return 1;
		// } else if (this.jahr == o.jahr && this.monat == o.monat && this.tag > o.tag)
		// {
		// return 1;
		// }

		// return -1;
	}
}
