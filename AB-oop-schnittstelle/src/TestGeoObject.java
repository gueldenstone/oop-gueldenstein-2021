public class TestGeoObject {
	public static void main(String[] args) {
		Rectangle r1 = new Rectangle(0, 0, 10.09, 20);
		Circle c1 = new Circle(0, 0, 40.5);

		System.out.printf("area of rectangle %.2f\n", r1.determineArea());
		System.out.printf("area of circle %.2f\n", c1.determineArea());

		// runtime polymorphism
		GeoObject[] forms = { r1, c1 };

		for (GeoObject o : forms) {
			System.out.printf("This object (%s) with the content %s has an area of %.2f\n", o.getClass(), o,
					o.determineArea());
		}
	}
}
