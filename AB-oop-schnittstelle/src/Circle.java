public class Circle extends GeoObject {
	private double radius;
	private static double pi = 3.14159;

	public Circle() {
		super(0.0, 0.0);
		this.radius = 0.0;
	}

	public Circle(double x, double y, double r) {
		super(x, y);
		this.radius = r;
	}

	public double getRadius() {
		return this.radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public

	/**
	 * Methode bestimmt die Flaeche des geometrischen Objekts
	 * 
	 * @return die Flaeche
	 */
	@Override double determineArea() {
		return this.getRadius() * this.getRadius() * Circle.pi;
	}

	@Override
	public String toString() {
		return String.format("[ position: [ x=%.3f, y=%.3f ], radius: %.3f ]", this.getX(), this.getY(), this.getRadius());
	}
}
